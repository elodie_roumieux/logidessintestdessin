﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace LogiDessinTestDessin
{
    public abstract class Figure : Panel
    {
        protected Point origine;
        protected Pen pen;
        protected Point? end;

        //Constructeurs
        public Figure()
        {
            this.origine = new Point(0, 0);
            this.pen = new Pen(Color.White);
        }

        public Figure(int x, int y)
        {
            this.origine = new Point(x, y);
            this.pen = new Pen(Color.Black);
        }

        public Figure(int x, int y, Pen p)
        {
            this.origine = new Point(x, y);
            this.pen = (Pen)p.Clone();
        }

        public Figure(int xo, int yo, int xend, int yend, Pen p)
        {
            this.origine = new Point(xo, yo);
            this.pen = p;
            this.end = new Point(xend, yend);
        }

        public Figure(int xo, int yo, int xend, int yend)
        {
            this.origine = new Point(xo, yo);
            this.end = new Point(xend, yend);
        }

        //Méthodes

        public override string ToString()
        {
            return "Coordinates of origin: (" + origine.X.ToString() + ", " + origine.Y.ToString() + ")\n Color: " + pen.ToString();
        }

        public Point getOrigine()
        {
            return origine;
        }

        public Point? getEnd()
        {
            return end;
        }

        public void setEnd(int x, int y)
        {
            this.end = new Point(x, y);
        }

        public void setEndToNull()
        {
            this.end =null;
        }

        public void setPen(Pen p)
        {
            this.pen = (Pen) p.Clone();
        }

        public abstract double getPerimeter();
        public abstract double getSurface();
        public abstract void draw(PaintEventArgs e, Pen p);


    }
}
