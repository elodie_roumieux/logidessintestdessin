﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace LogiDessinTestDessin
{
    class Segment : Figure
    {
        

        public Segment()
            : base()
        {
        }

        public Segment(int x, int y)
            : base(x, y)
        {
        }

        public Segment(int x, int y, Pen p)
            : base(x, y, p)
        {
        }

        public Segment(int xorigine, int yorigine, int xend, int yend)
            : base(xorigine, yorigine, xend, yend)
        {
        }

        public Segment(int xorigine, int yorigine, int xend, int yend, Pen p)
            : base(xorigine, yorigine, xend, yend, p)
        {
        }

        //Méthodes:

        public override double getPerimeter()
        {
            if (end.HasValue)
            {
                //returns segment length
                return System.Math.Sqrt(System.Math.Pow((end.Value.Y - origine.Y), 2) + System.Math.Pow((end.Value.X - origine.X), 2));
            }
            else return 0;
        }

        public override double getSurface()
        {
            throw new NotImplementedException();
        }

        public override void draw(PaintEventArgs e, Pen p)
        {
            if (!end.HasValue)
            {
                return;
            }
            if(pen!=null){
                p = pen;
            }           
            e.Graphics.DrawLine(p,origine,end.Value);
        }
    }
}
