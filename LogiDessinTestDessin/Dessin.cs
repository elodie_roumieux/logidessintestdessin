﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogiDessinTestDessin
{
    class Dessin : Panel
    {
        private List<List<Figure>> ListOfFiguresList;
        private Pen currentPen;
        private string figureName;
        private int xMouse;
        private int yMouse;
        public bool shouldDraw;

        public Dessin()
        {
            this.ListOfFiguresList = new List<List<Figure>>();
            this.currentPen = new Pen(Color.Black);
            this.figureName = "Segment";
            this.MouseClick += Dessin_MouseClick;
            this.MouseDoubleClick += Dessin_MouseDoubleClick;
            this.shouldDraw = true;

        }

        //Méthodes
        public void changePenColor(Color newColorOfPen)
        {
            currentPen.Color = newColorOfPen;
            if (ListOfFiguresList.Last<List<Figure>>().Count != 0)
            {
                ListOfFiguresList[ListOfFiguresList.Count - 1].Last<Figure>().setPen(currentPen);
            }
        }

        public void changePenThickness(float newThicknessOfPen)
        {
            currentPen.Width = newThicknessOfPen;
            //ListOfFiguresList[ListOfFiguresList.Count - 1].setPen(currentPen);
        }

        public void changeName(string newName)
        {
            this.figureName = newName;
        }

        public void ResetDessin()
        {
            this.ListOfFiguresList.Clear();
        }

        //Drawing methods

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            //set background white
            this.BackColor = Color.White;
            
            foreach(List<Figure> FiguresList in ListOfFiguresList){
                foreach (Figure figure in FiguresList)
                {
                    Console.WriteLine(figure.ToString());
                    figure.draw(e, currentPen); //? ne va pas tout mettre en noir puis tout en bleu, ... ?
                }
            }
        }

        public void DeleteLastFigure()
        {
            if (ListOfFiguresList.Count != 0)
            {

                var FiguresList = ListOfFiguresList[ListOfFiguresList.Count - 1];

                if (FiguresList.Count == 0)
                {
                    ListOfFiguresList.RemoveAt(ListOfFiguresList.Count - 1);
                    DeleteLastFigure();
                    return;
                }

                if (FiguresList.Count == 1)
                {
                    ListOfFiguresList.RemoveAt(ListOfFiguresList.Count - 1);
                    return;
                }

                FiguresList.RemoveAt(FiguresList.Count - 1);//delete origine
                FiguresList[FiguresList.Count - 1].setEndToNull(); //set previous end to null
                //+ stocker ce point à un endroit pour le redo?
               

                this.Refresh();
            }
            
        }

        //Methods on Mouse events

        private void Dessin_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (shouldDraw)
            {
                this.xMouse = e.X;
                this.yMouse = e.Y;
                if (this.ListOfFiguresList.Count != 0)
                {
                    var FiguresList = ListOfFiguresList[ListOfFiguresList.Count - 1];
                    if (FiguresList.Count!=0 && FiguresList[FiguresList.Count - 1].getEnd() == null)
                    {
                        FiguresList[FiguresList.Count - 1].setEnd(xMouse, yMouse);
                    }
                    FiguresList.Add(new Segment(xMouse, yMouse, currentPen));
                }
                else
                {
                    this.ListOfFiguresList.Add(new List<Figure>());
                    this.ListOfFiguresList[0].Add(new Segment(xMouse, yMouse, currentPen));
                }
                this.Refresh();
                Console.WriteLine("" + e.X + " y:" + e.Y);
            }
            else
            {
                //faire la séléction d'une figure pour afficher ses carac.
            }

        }

        private void Dessin_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (shouldDraw)
            {
                if (this.ListOfFiguresList.Count != 0)
                {
                    var FiguresList = ListOfFiguresList[ListOfFiguresList.Count - 1];
                    FiguresList[FiguresList.Count - 1].setEnd(ListOfFiguresList[ListOfFiguresList.Count-1][0].getOrigine().X, ListOfFiguresList[ListOfFiguresList.Count-1][0].getOrigine().Y);
                    this.ListOfFiguresList.Add(new List<Figure>());                   
                }
                this.Refresh();
            }
        }
    }
}
