﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogiDessinTestDessin
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void NewButton_Click(object sender, EventArgs e)
        {
            //ask if want to save
            this.drawingScreen.ResetDessin();
            this.drawingScreen.Refresh();
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {

        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            this.statusStrip1.Visible = true;
            this.toolStripProgressBar1.Visible = true;
            this.toolStripStatusLabel1.Visible = true;
            this.toolStripStatusLabel1.Text = "Enregistrement en cours";
        }

        /// <summary>
        /// Shows the color dialog and wait for user to press ok.
        /// </summary>
        private void ColorButton_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                this.drawingScreen.changePenColor(colorDialog1.Color);
            }
        }

        private void ThicknessButton_Click(object sender, EventArgs e)
        {
            //this.drawingScreen.changePenThickness(newThicknessOfPen);
        }

        private void ClickButton_Click(object sender, EventArgs e)
        {
            this.drawingScreen.shouldDraw = false;
        }

        private void DrawingButton_Click(object sender, EventArgs e)
        {
            this.drawingScreen.shouldDraw = true;
        }

        private void QuitMenuItem_Click(object sender, EventArgs e)
        {
            //demander si l'on sauvegrde/annule
            Environment.Exit(0);       
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.drawingScreen.DeleteLastFigure();
        }

    }
}
